package com.web.services.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.services.rest.model.Posts;

public interface PostRepository extends JpaRepository<Posts, Integer> {

}
