package com.web.services.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.services.rest.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
