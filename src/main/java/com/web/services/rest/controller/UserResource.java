package com.web.services.rest.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.web.services.rest.dao.PostRepository;
import com.web.services.rest.dao.UserRepository;
import com.web.services.rest.exception.PostNotFoundException;
import com.web.services.rest.exception.UserNotFoundException;
import com.web.services.rest.model.Posts;
import com.web.services.rest.model.User;

@RestController
public class UserResource {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PostRepository postRepository;

	@GetMapping("/users")
	public  List<User> getAllUsers(){
		return userRepository.findAll();
	}

	@GetMapping("/users/{id}")
	public Optional<User> retrieveUser(@PathVariable int id) {
		Optional<User> user = userRepository.findById(id);
		if(!user.isPresent()) {
			throw new UserNotFoundException("The requested user "+id+" not found");
		}
		return user;

	}

	@PostMapping("/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User addedUser = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(addedUser.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}


	@DeleteMapping("/users/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		this.retrieveUser(id);
		userRepository.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	/*
	@GetMapping("/users/{userId}/posts")
	public ResponseEntity<List<Posts>> getPosts(@PathVariable(value = "userId") int id){
		Optional<User> user = userRepository.findById(id);
		if(!user.isPresent()) {
			throw new UserNotFoundException("The requested user "+id+" not found");
		}
		List<Posts> posts = user.get().getPosts();
		if(posts.isEmpty()) {
			return new ResponseEntity<List<Posts>>(posts, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Posts>>(posts, HttpStatus.OK);

	}
	 */

	@GetMapping("/users/{userId}/posts")
	public List<Posts> getPosts(@PathVariable(value = "userId") int id){
		Optional<User> user = userRepository.findById(id);
		if(!user.isPresent()) {
			throw new UserNotFoundException("The requested user "+id+" not found");
		}
		List<Posts> posts = user.get().getPosts();
		return posts;
	}

	@PostMapping("/users/{userId}/posts")
	public ResponseEntity<Object> createPosts(@PathVariable(value = "userId") int id, @Valid @RequestBody Posts post){
		Optional<User> userOptional = userRepository.findById(id);
		if(!userOptional.isPresent()) {
			throw new UserNotFoundException("The requested user "+id+" not found");
		}
		User user = userOptional.get();
		post.setUser(user);
		Posts createdPost = postRepository.save(post);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdPost.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/users/{userId}/posts/{id}")
	public Posts retrievePost(@PathVariable(value = "userId") int userId, @PathVariable(value = "id") int id){
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent()) {
			throw new UserNotFoundException("The requested user "+userId+" not found");
		}

		Optional<Posts> postOptional = postRepository.findById(id);
		if(!postOptional.isPresent()) {
			throw new PostNotFoundException("The requested post "+id+" not found");
		}

		Posts post = postOptional.get();
		if (post.getUser().getId() != userId) {
			throw new PostNotFoundException("The requested post "+id+" not found for the user "+userId);
		}
		return postOptional.get();
	}

}
