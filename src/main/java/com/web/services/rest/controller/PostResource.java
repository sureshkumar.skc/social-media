package com.web.services.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.services.rest.dao.PostRepository;
import com.web.services.rest.model.Posts;

@RestController
public class PostResource {
	
	
	@Autowired
	private PostRepository postRepository;

	@GetMapping("/posts")
	public List<Posts> getAllPosts(){
		return postRepository.findAll();
	}

}
