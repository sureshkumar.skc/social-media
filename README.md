# Links

### H2-console - /h2-console
- JDBC URL: jdbc:h2:mem:testdb

### Swagger
- swagger ui - /swagger-ui.html
- swagger api docs - /v2/api-docs

